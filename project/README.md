1. Create a `.env` from the `.env.dist` file. Adapt it to the database Link

        I added .env.local file to git repository to make it run without issue

2. Login/Registration form is not working, you can test by running this command inside container to generate dummy values

    ```bash
        $ php bin/console make:doctrine:fixtures:load 
    ```

