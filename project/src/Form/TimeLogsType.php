<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\TimeLogs;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeLogsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startAt',DateTimeType::class,[
                'date_label' => 'Started At',
                'data'=> \DateTimeImmutable::createFromFormat('Y-m-d H:i', date('Y-m-d H:i')),
                'input' => 'datetime_immutable',
                'widget'=> 'single_text',
                'input_format' => 'Y-m-d H:i',


            ])
            ->add('finishAt',DateTimeType::class,[
                'date_label' => 'Finished aT',
                'data'=> \DateTimeImmutable::createFromFormat('Y-m-d H:i', date('Y-m-d H:i')),
                'input' => 'datetime_immutable',
                'widget'=> 'single_text',
                'input_format' => 'Y-m-d H:i'
            ])
            ->add('user',EntityType::class,[
                'class'=>User::class,
                'choice_label'=>'userName'
            ])
            ->add('project',EntityType::class,[
                'class'=>Project::class,
                'choice_label'=>'title',
                'disabled' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TimeLogs::class,
        ]);
    }
}
