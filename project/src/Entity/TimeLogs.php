<?php

namespace App\Entity;

use App\Repository\TimeLogsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 *
 * @ORM\Table(name="time_logs",
 *    uniqueConstraints={
 *        @ORM\UniqueConstraint(name="time_logs_unique_key",
 *            columns={"start_at", "finish_at"})
 *    }
 * )
 * @ORM\Entity(repositoryClass=TimeLogsRepository::class)
 */
class TimeLogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $finishAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="timeLogs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="timeLogs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getFinishAt(): ?\DateTimeImmutable
    {
        return $this->finishAt;
    }

    public function setFinishAt(\DateTimeImmutable $finishAt): self
    {
        $this->finishAt = $finishAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getDuration()
    {

        return $this->getFinishAt()->diff($this->getStartAt());
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getDuration()->i === 0 && $this->getDuration()->h === 0) {
            $context->buildViolation('A task duration should be at least 1 minute')
                ->atPath('startAt')
                ->addViolation();
            $context->buildViolation('A task duration should be at least 1 minute')
                ->atPath('finishAt')
                ->addViolation();
        }
        if ($this->getDuration()->days > 0) {
            $context->buildViolation('Task should be tracking a 1 day schedule')
                ->atPath('startAt')
                ->addViolation();
            $context->buildViolation('Task should be tracking a 1 day schedule')
                ->atPath('finishAt')
                ->addViolation();
        }

    }
}
