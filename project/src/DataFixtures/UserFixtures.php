<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setUsername('wael');
        $user->setPassword($this->userPasswordEncoder->encodePassword($user,'test'));
        $manager->persist($user);
        $user2 = new User();
        $user2->setUsername('testUser');
        $user2->setPassword($this->userPasswordEncoder->encodePassword($user2,'test'));
        $manager->persist($user2);
        $manager->flush();
    }
}
