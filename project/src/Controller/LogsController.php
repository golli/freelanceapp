<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\TimeLogs;
use App\Form\ProjectFormType;
use App\Form\TimeLogsType;
use App\Form\UserLoginFormType;
use App\Repository\TimeLogsRepository;
use App\Traits\RGBcodeGenerator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class LogsController extends AbstractController
{
    use RGBcodeGenerator;
    /**
     * @var ChartBuilderInterface
     */
    private $chartBuilder;

    public function __construct(ChartBuilderInterface $chartBuilder)
    {

        $this->chartBuilder = $chartBuilder;
    }
    /**
     * @Route("/project/{id}/logs", name="app_project_logs_list")
     */
    public function list(Project $project,TimeLogsRepository $timeLogsRepository): Response
    {
        $logs=$timeLogsRepository->findAll();
        $chart = $this->generateChart($logs);
        return $this->render('logs/list.html.twig', [
            'projectId' => $project->getId(),
            'logs' => $logs,
            'chart' => $chart,
        ]);
    }

    /**
     * @Route("/project/{id}/logs/add", name="app_project_logs_list_add")
     */
    public function add(Project $project,Request $request,EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TimeLogsType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var TimeLogs $timeLogs */
            $timeLogs = $form->getData();
            $timeLogs->setProject($project);
            $entityManager->persist($timeLogs);
            $entityManager->flush();
            $this->addFlash('success', 'new sprint Added');
            return $this->redirectToRoute('app_project_logs_list', [
                'id'=>$project->getId()
            ]);
        }

        return $this->render('logs/add.html.twig', [
            'logForm'=>$form->createView(),
            'project'=>$project
        ]);
    }
    /**
     * @Route("/project/{id}/logs/edit", name="app_logs_edit")
     */
    public function edit(TimeLogs $timeLogs,Request $request,EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TimeLogsType::class,$timeLogs);
        $project=$timeLogs->getProject();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var TimeLogs $timeLogs */
            $timeLogs = $form->getData();
            $entityManager->persist($timeLogs);
            $entityManager->flush();
            $this->addFlash('success', 'new sprint Added');
            return $this->redirectToRoute('app_project_logs_list', [
                'id'=>$project->getId()
            ]);
        }

        return $this->render('logs/edit.html.twig', [
            'logForm'=>$form->createView(),
            'project'=>$project
        ]);
    }
    /**
     * @Route("/logs/{id}/delete", name="app_logs_delete")
     */
    public function delete(TimeLogs $timeLogs,TimeLogsRepository $timeLogsRepository,EntityManagerInterface $entityManager): Response
    {
        $project=$timeLogs->getProject();
        $entityManager->remove($timeLogs);
        $entityManager->flush();
        return $this->redirectToRoute('app_project_logs_list', [
            'id'=>$project->getId()
        ]);
    }
    private function generateChart(array $projectTasksList): Chart
    {
        $participants=[];
        $minutes=[];
        $backgroundColors=[];
        /** @var TimeLogs $task */
        foreach ($projectTasksList as $task){
            $index = array_search($task->getUser()->getUsername(), $participants);
            if ($index === false)
            {
                $index =array_push($participants,$task->getUser()->getUsername());
                $backgroundColors[] = $this->getRandomRGBColorCode();
                $minutes[$index-1] =  $task->getDuration()->h * 60 + $task->getDuration()->i;
            }else{

                $minutes[$index] =$minutes[$index]+ $task->getDuration()->h * 60 + $task->getDuration()->i;
            }


        }

        $chart = $this->chartBuilder->createChart(Chart::TYPE_PIE);

        $chart->setData([
            'labels' => $participants,
            'datasets' => [
                [
                    'data' => $minutes,
                    'backgroundColor' => $backgroundColors,
                    'hoverOffset' => 1,
                ],
            ],
        ]);

        return $chart;

    }

}
