<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\TimeLogs;
use App\Form\ProjectFormType;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class ProjectController extends AbstractController
{


    /**
     * @Route("/", name="app_project_list")
     */
    public function list(ProjectRepository $projectRepository): Response
    {
        $projectList = $projectRepository->findAll();
        return $this->render('project/list.html.twig', [
            'projects' => $projectList,
        ]);
    }

    /**
     * @Route("/project", name="app_project_new")
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProjectFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $project = $form->getData();
            $entityManager->persist($project);
            $entityManager->flush();
            $this->addFlash('success', 'new project Added');
            return $this->redirectToRoute('app_project_list', [
            ]);
        }

        return $this->render('project/add.html.twig', [
            'controller_name' => 'ProjectController',
            'projectForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/project/{id}/export", name="app_project_export")
     */
    public function export(Project $project, Request $request, EntityManagerInterface $entityManager): Response
    {
        $timeLogs = $project->getTimeLogs()->toArray();

        $data[] = [
            'project' => 'project_Title',
            'user' => 'user',
            'start At' => 'start_At',
            'Finish At' => 'Finish_At'
        ];
        $fp = fopen('php://temp', 'w');
        foreach ($timeLogs as $log) {
            $data[] = [
                'project' => $project->getTitle(),
                'user' => $log->getUser()->getUsername(),
                'start At' => $log->getStartAt()->format('d/m/Y'),
                'Finish At' => $log->getFinishAt()->format('d/m/Y')];
            foreach ($data as $fields) {
                fputcsv($fp, $fields);
            }
        }


        rewind($fp);
        $response = new Response(stream_get_contents($fp));
        fclose($fp);

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $project->getTitle() . '"_tasks_logs.csv');

        return $response;
    }


}
