<?php

namespace App\Traits;

trait RGBcodeGenerator
{

    function getRandomRGBColorCode(): string
    {
        return 'rgb(' . rand(0, 255) . ',' . rand(0, 255) . ',' . rand(0, 255) . ')'; #using the inbuilt random function
    }
}